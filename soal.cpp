/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <list>
#include <iterator>
#include <stdlib.h>

using namespace std;

//NilaiType, terdiri dari integer UTS dan UAS
class NilaiType
{
public:
  int UTS;
  int UAS;
};

//Mahasiswa, terdiri dari integer NPM dan NilaiType
class Mahasiswa
{
public:
  int NPM;
  NilaiType nilaiType;
};

//Kelas, dengan array 100 Mahasiswa
class Kelas
{
    public:
        list < Mahasiswa > mahasiswaArray;




    public:
      void InitKelas (int totalMahasiswa)
      {
        for (int i = 0; i < totalMahasiswa; i++)
        {
            Mahasiswa mahasiswa;
            mahasiswa.NPM = 0;
        	mahasiswaArray.push_back(mahasiswa);
        }
      };
      
    public:
        void InsertMhs (Mahasiswa mahasiswa) {
            mahasiswaArray.push_back(mahasiswa);
        }
        
    public:
        int NbMhs(){
            return mahasiswaArray.size();
        }
        
    public:
        string IsEmpty(){
            if (mahasiswaArray.empty()==0){
                return "false";
            }else {
                return "true";
            }
        }

};

void
initKelas ()
{
  cout << "initKelas";
};

//Set Nilai UTS dan UAS, return NilaiType
NilaiType
SetNilai (int UTS, int UAS)
{
  NilaiType nilaiType;
  nilaiType.UTS = UTS;
  nilaiType.UAS = UAS;

  return nilaiType;
};

//Set NPM dan Nilai Type Mahasiwa, return Mahasiswa 
Mahasiswa
SetMahasiswa (int NPM, NilaiType nilaiType)
{
  Mahasiswa mahasiswa;
  mahasiswa.NPM = NPM;
  mahasiswa.nilaiType = nilaiType;

  return mahasiswa;
}

double
hitungRataRata (Mahasiswa mahasiswa)
{
  double rataRata = (mahasiswa.nilaiType.UTS + mahasiswa.nilaiType.UAS) / 2;

  return rataRata;
}

void cetakDiatasLimaPuluh(Kelas kelasN) {
    for (Mahasiswa mhs : kelasN.mahasiswaArray) {
        // cout << mhs.NPM << ":" << hitungRataRata(mhs) <<'\n';
        if (hitungRataRata(mhs) > 50){
            cout << mhs.NPM << '\n';
        }
    }
}

double rataRataKelas (Kelas kelasN) {
    double total = 0;
    int jumlahMahasiswa = 0;
    for (Mahasiswa mhs : kelasN.mahasiswaArray) {
        total = total + hitungRataRata(mhs);
        jumlahMahasiswa = jumlahMahasiswa +1;
    }
    
    return (total/jumlahMahasiswa);
}


int
main ()
{
  //Inisialisasi Kelas, berisi 100 Mahasiswa -> NPM=0;
  Kelas kelas;
  //Test IsEmpty
  kelas.InitKelas(100);
  
  
  

  //Test SetNilai
  NilaiType nilai;
  nilai = SetNilai (85, 20); //Input nilai UTS dan UAS

  //Test SetMahasiswa
  Mahasiswa mahasiswaBaru;
  mahasiswaBaru = SetMahasiswa (12344321, nilai);

  //Test hitungRataRata
  cout <<  "Rata-rata UTS dan UAS:" << hitungRataRata (mahasiswaBaru) << "\n";
  
  cout << "Jumlah mahasiswa:" << kelas.NbMhs() << "\n";
  
  //Test IsEmpty
  cout << "Kelas Kosong? " << kelas.IsEmpty() << "\n";

  //Test InsertMahasiswa
  kelas.InsertMhs(mahasiswaBaru);
  
  //Check Mahasiswa yang Baru ditambahkan
  cout << "Jumlah mahasiswa:" << kelas.NbMhs() << "\n";
  
  Kelas kelasA;
  int npm = 1000000;
  int n = 100;
  int m = 0;
//   Isi NPM dan Nilai Mahasiswa dalam kelas
  for (int i =0; i< 100; i++) {
      Mahasiswa mahasiswa;
      mahasiswa.NPM=(npm+i);
      mahasiswa.nilaiType.UTS=((rand()% 100));
      mahasiswa.nilaiType.UAS=((rand()% 100));
      kelasA.InsertMhs(mahasiswa);
  }
  
  //Test Cetak NPM Mahasiswa nilai >50 
  cout << "\n";
  cetakDiatasLimaPuluh(kelasA);
  
  //Rata-rata kelas
  cout <<"Rata-rata Kelas: " << rataRataKelas(kelasA);
  
  return 0;
}
